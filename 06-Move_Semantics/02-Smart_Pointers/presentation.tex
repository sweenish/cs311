\documentclass[10pt]{beamer}
\usetheme{metropolis}

\usepackage{url}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage{minted}

\AtBeginSection[]{ %
\begin{frame}
	\vfill{}
	\centering
	\textbf{\LARGE{\insertsectionhead}}
	\vfill{}
\end{frame}
}

\title{Smart Pointers}
\subtitle{CS 311}
\author{Adam Sweeney}
\institute{Wichita State University, EECS}

\begin{document}
\begin{frame}
	\titlepage{}
\end{frame}

\begin{frame}
	\frametitle{Introduction}
	\begin{itemize}
		\item Communicating move semantics used the case of a ``smart'' pointer class
		\item We'll actually discuss smart pointers as implemented in C++11
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Agenda}
	\begin{itemize}
		\item A need
		\item \texttt{unique\_ptr}
		\item \texttt{shared\_ptr} \& \texttt{weak\_ptr}
		\item Code files taken from \url{www.learncpp.com}
	\end{itemize}
\end{frame}

\section{A need}
\begin{frame}
	\frametitle{A need}
	\begin{itemize}
		\item Pointers are powerful tools
		\item They can also be a pain
		\item Even if we program well, mistakes happen
		\item Nobody likes memory leaks
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Thinking About Dealing with Pointers}
	\begin{itemize}
		\item As our programs grow in complexity, we may find that we rarely delete the pointer where it was created
		\item This makes the already difficult task of remembering to delete all pointers much tougher
		\item Let alone accounting for the consequences of early termination or throwing an exception
		\item It would be nice if our pointers could clean up after themsevles
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Resource Acquisition Is Initialization}
	\begin{itemize}
		\item Also known as RAII
		\item This is a programming technique
		\item The principle at play ties resource use to the lifetime of an object with automatic 
		duration
		\begin{itemize}
			\item A statically declared object has automatic duration
			\item This principle is realized through classes with constructors and destructors
		\end{itemize}
		\item Our resource will be generated at the time of object construction
		\item Our resource will be available so long as the object is available
		\item Our resource is properly returned/destroyed when our object goes out of scope
		\begin{itemize}
			\item The destructor is responsible for this
		\end{itemize}
		\item The key here is that destructors are \textit{always} invoked when an object goes out of 
		scope, regardless of how they go out of scope
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{C++11 and Smart Pointers}
	\begin{itemize}
		\item Among the many changes that C++11 brought were more smart pointer classes
		\item We say more because a smart pointer class was attempted in C++03, \texttt{auto\_ptr()}
		\begin{itemize}
			\item It was rather limited
			\item It used move semantics before they were officially available in the language, which made the implementation sloppy
			\item It could not handle the deletion of a dynamically allocated array
			\item It has been deprecated, and was removed in C++17
		\end{itemize}
		\item A new class, \texttt{unique\_ptr$<>$}, replaces \texttt{auto\_ptr()}
		\item Two other new classes were introduced
		\begin{itemize}
			\item \texttt{shared\_ptr()}
			\item \texttt{weak\_ptr()}
		\end{itemize}
	\end{itemize}
\end{frame}

\section{\texttt{unique\_ptr$<>$}}
\begin{frame}
	\frametitle{\texttt{unique\_ptr$<>$}}
	\begin{itemize}
		\item This is the most direct replacement to \texttt{auto\_ptr()}
		\item This is likely the smart pointer you will use the most
		\item As its name implies, it is only to be used to manage a dynamically allocated object that is not shared by multiple objects
		\begin{itemize}
			\item To reiterate, \texttt{unique\_ptr$<>$} has complete ownership of its object
			\item It contains the only pointer to that object, ever
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{A Short Example}
	\begin{minted}[gobble=2, tabsize=4, linenos, fontsize=\footnotesize]{c++}
		#include <iostream>
		#include <memory> // for std::unique_ptr
		 
		class Resource
		{
		public:
			Resource() { std::cout << "Resource acquired\n"; }
			~Resource() { std::cout << "Resource destroyed\n"; }
		};
		 
		int main()
		{
			// allocate a Resource object and have it owned by unique_ptr
			std::unique_ptr<Resource> res(new Resource);
		 
			return 0;
		} // res goes out of scope here, the allocated Resource is destroyed
	\end{minted}
	\begin{itemize}
		\item This can be found in \texttt{unique01.cpp}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Observations}
	\begin{itemize}
		\item The \texttt{unique\_ptr$<>$} object is declared statically (will be allocated on the stack)
		\begin{itemize}
			\item Dynamic declaration would ruin the point
		\end{itemize}
		\item Because the \textit{static} object is guaranteed to go out of scope at some point, the destructor \textit{will} get invoked
		\begin{itemize}
			\item This will ensure that the pointer is properly disposed of, \textit{automatically}
		\end{itemize}
		\item \texttt{unique\_ptr$<>$} implements correct move semantics
		\begin{itemize}
			\item It also disables copy initialization and copy assignment
			\item This means that move semantics must be used in order to transfer the pointer
		\end{itemize}
		\item Refer to \texttt{unique02.cpp}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{A Tangent: Notes on \texttt{unique02.cpp}}
	\begin{itemize}
		\item \mintinline[fontsize=\small]{c++}{(static_cast<bool>(res1) ? "not null\n" : "null\n")}
		\begin{itemize}
			\item This is known as the conditional, or ternary operator
			\item Ternary simply means that it requires three operands
			\item The operator itself is \texttt{?:}
		\end{itemize}
		\item The breakdown goes like this:\\
		\texttt{[conditional statement] ? [action if true] : [action if false]}
		\item We can also oberve that we are able to statically cast a \texttt{unique\_ptr$<>$} to \texttt{bool}
		\item It evaluates to \texttt{TRUE} if the \texttt{unique\_ptr$<>$} is pointing to something, and \texttt{FALSE} if the 
		\texttt{unique\_ptr$<>$} contains \texttt{nullptr}
		\item Note the need to use \texttt{std::move()} to transfer ownership of the pointer
		\begin{itemize}
			\item Copy semantics are completely disabled for \texttt{unique\_ptr$<>$}
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{A Better Way to Declare \texttt{unique\_ptr$<>$}}
	\begin{itemize}
		\item Introduced in C++14, the function \texttt{make\_unique$<>$()} is a cleaner way to create objects to store in a 
		\texttt{unique\_ptr$<>$}
		\item Refer to \texttt{unique03.cpp}
		\item We can see that the function is templated
		\item It will construct an argument of the template type, and initializes it with the arguments passed into 
		\texttt{make\_unique$<>$()}
		\item The use of \texttt{make\_unique$<>$()} is optional, but strongly recommended
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Returning \texttt{unique\_ptr$<>$} From a Function}
	\begin{itemize}
		\item This is a thing, and it can be done
	\end{itemize}
	\begin{minted}[gobble=1, tabsize=4, fontsize=\footnotesize]{c++}
		std::unique_ptr<Resource> createResource()
		{
			 return std::make_unique<Resource>();
		}
		 
		int main()
		{
			std::unique_ptr<Resource> ptr = createResource();
		 
			// do whatever
		 
			return 0;
		}
	\end{minted}
	\begin{itemize}
		\item A \texttt{unique\_ptr$<>$} should always be returned by value$^*$
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Passing \texttt{unique\_ptr$<>$} to a Function}
	\begin{itemize}
		\item This is also a thing, and it can be done
		\item If you pass the \texttt{unique\_ptr$<>$} by value:
		\begin{itemize}
			\item The function will take ownership of the resource
			\item Use \texttt{std::move()} when giving the \texttt{unique\_ptr$<>$} to a function
			\item Refer to \texttt{unique04.cpp}
		\end{itemize}
		\item Generally, we don't care to pass the entire \texttt{unique\_ptr$<>$}, because what we care about is the 
		resource being managed
		\item We can pass a pointer to the resource being managed instead
		\begin{itemize}
			\item This uses the \texttt{unique\_ptr$<>$} member function \texttt{get()}
		\end{itemize}
		\item Refer to \texttt{unique05.cpp}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Pitfalls}
	\begin{itemize}
		\item Do NOT let mulitple classes manage the same object
		\item Do NOT manually delete the resource being managed
	\end{itemize}
\end{frame}

\section{\texttt{shared\_ptr$<>$}}
\begin{frame}
	\frametitle{\texttt{shared\_ptr$<>$}}
	\begin{itemize}
		\item Sometimes you need multiple pointers to a single resource
		\item By using \texttt{shared\_ptr$<>$}, multiple smart pointers can co-own a resource
		\item So long as a single \texttt{shared\_ptr$<>$} is still owning the resource, the resource will not be 
		destroyed as other \texttt{shared\_ptr$<>$} objects go out of scope
		\item Refer to \texttt{shared01.cpp}
		\item Note that the Resource is not destroyed until the final \texttt{shared\_ptr$<>$} object goes out of scope
		\item Interestingly, \texttt{make\_shared$<>$()} is available since C++11
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Some Details on \texttt{shared\_ptr$<>$}}
	\begin{itemize}
		\item \texttt{shared\_ptr$<>$} uses two internal pointers
		\item One points to the resource being managed
		\item The other points to a dynamically allocated control block
		\begin{itemize}
			\item The control block tracks, among other things, how many \texttt{shared\_ptr$<>$} objects are managing 
			a resource
		\end{itemize}
		\item When individually declared, the pointer to the resource and pointer to the control block are two separate 
		allocations
		\begin{itemize}
			\item Using \texttt{make\_shared$<>$()}, it is done in a single allocation
		\end{itemize}
		\item Individually declaring two \texttt{shared\_ptr$<>$} objects to the same resource can get us into trouble
		\begin{itemize}
			\item They will each create their own control block, and those control blocks will each think that they are the 
			only \texttt{shared\_ptr$<>$} owning the resource
			\item Destroying one will cause the resource to be destroyed, ruining any remaining \texttt{shared\_ptr$<>$} 
			objects
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{More Details}
	\begin{itemize}
		\item If we do need to have multiple owners of a resource, we should do so through copy assignment
		\begin{itemize}
			\item This helps guarantee that the control block updates properly
		\end{itemize}
		\item Shared pointers can be created from \texttt{unique\_ptr$<>$} objects
		\begin{itemize}
			\item There is a specific constructor for this
		\end{itemize}
		\item Unique pointers can NOT be created from \texttt{shared\_ptr$<>$} objects
		\item Pitfalls are similar to those of \texttt{unique\_ptr$<>$}
		\item There is one other, though
	\end{itemize}
\end{frame}

\section{Circular Dependency Issues}
\begin{frame}
	\frametitle{Circular Dependency Issues}
	\begin{itemize}
		\item Refer to \texttt{shared02.cpp}
		\item Note that the destructors were never invoked
		\begin{itemize}
			\item The \texttt{shared\_ptr$<>$} data member in each object prevented the other object from 
			being destroyed
		\end{itemize}
		\item This is not ideal behavior
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Enter \texttt{weak\_ptr$<>$}}
	\begin{itemize}
		\item These exist solely to solve this issue
		\item A \texttt{weak\_ptr$<>$} object is an ``observer,'' it sees and can access the same object as a 
		\texttt{shared\_ptr$<>$}, but it is NOT an owner
		\item So, when \texttt{shared\_ptr$<>$} is checking for other owners, \texttt{weak\_ptr$<>$} objects 
		don't count
		\item Refer to \texttt{weak01.cpp}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\texttt{weak\_ptr$<>$} is Weak}
	\begin{itemize}
		\item While this appears to completely solve our problem, there is one wrinkle
		\item A \texttt{weak\_ptr$<>$} does not have an overloaded arrow operator, so they are not directly 
		usable
		\item If we want to use a weak pointer, we must first convert it to a \texttt{shared\_ptr$<>$}
		\item This is accomplished through the \texttt{weak\_ptr$<>$} member function \texttt{lock()}
		\item Refer to \texttt{weak02.cpp}
	\end{itemize}
\end{frame}

\end{document}
