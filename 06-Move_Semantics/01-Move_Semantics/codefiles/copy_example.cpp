#include <iostream>

// "Smart" pointer class declared and implemented in palce
template<class T>
class Auto_ptr
{
	T* m_ptr;	// Private data; classes are private by default
public:
	Auto_ptr(T* ptr = nullptr)
		:m_ptr(ptr)
	{
		std::cout << "Auto_ptr parameterized ctor\n";
	}
 
	~Auto_ptr()
	{
		std::cout << "Auto_ptr destructor\n";
		delete m_ptr;
	}
 
	// Copy constructor
	// Do deep copy of a.m_ptr to m_ptr
	Auto_ptr(const Auto_ptr& a)
	{
		std::cout << "Auto_ptr Copy Constructor\n";
		m_ptr = new T;
		*m_ptr = *a.m_ptr;
	}
 
	// Copy assignment
	// Do deep copy of a.m_ptr to m_ptr
	Auto_ptr& operator=(const Auto_ptr& a)
	{
		std::cout << "Auto_ptr Copy Assignment\n";
		// Self-assignment detection
		if (&a != this) {
			// Release any resource we're holding
			delete m_ptr;
	
			// Copy the resource
			m_ptr = new T;
			*m_ptr = *a.m_ptr;
		}
 
		return *this;
	}
 
	T& operator*() const { return *m_ptr; }
	T* operator->() const { return m_ptr; }
	bool isNull() const { return m_ptr == nullptr; }
};

// Another class to illustrate our point
class Resource
{
public:
	Resource()  { std::cout << "Resource acquired\n"; }
	~Resource() { std::cout << "Resource destroyed\n"; }
};

// A non-class function
Auto_ptr<Resource> generateResource()
{
	std::cout << "*** In generateResource()\n";
	Auto_ptr<Resource> res(new Resource);	// this will invoke the parameterized constructor
	return res;		// this will invoke the copy constructor
}

int main()
{
	std::cout << "*** In main()\n";
	Auto_ptr<Resource> mainres;
	std::cout << "*** Leaving main()\n";
	mainres = generateResource(); // this assignment will invoke the copy assignment
	std::cout << "*** Back in main()\n";
	std::cout << "*** End program\n";
 
	return 0;
}

/*
 * Running the code, we can observe a surprising amount of extra work going on in the background. Notably in the 
 * function generateResource(). What's happening is that the compiler is making all the necessary copies that we have 
 * required in our code. This serves to illustrate how inefficient copy semantics alone can be. 
 */