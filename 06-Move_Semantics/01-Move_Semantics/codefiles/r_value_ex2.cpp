#include <iostream>
 
int main()
{
    int &&rref = 5; // because we're initializing an r-value reference with a literal, a temporary with value 5 is created here
    rref = 10;      // So, I did not just say 5 = 10; I changed the value of the temporary that the r-value reference is referring to
    std::cout << rref << std::endl;
 
    return 0;
}