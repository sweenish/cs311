#include <iostream>

/*
 * This example shows that we can overload a function based the value category of the parameter
 * In this case, the sole distinguishing feature between the fun() signatures is the value category of the parameter
 * In the first instance, the parameter category is an l-value, and the second the value category is an r-value
 * The keyword const, according the standard, does not play a role; however the compiler may still make a distinction
 */

void fun(const int &lref) // l-value arguments will select this function
{
	std::cout << "l-value reference to const\n";
}
 
void fun(int &&rref) // r-value argument will select this function
{
	std::cout << "r-value reference\n";
}
 
int main()
{
	int x = 5;
	fun(x); // l-value argument calls l-value version of function
	fun(5); // r-value argument calls r-value version of function
 
	return 0;
}