#include <iostream>
 
template<class T>
class Auto_ptr
{
	T* m_ptr;
public:
	Auto_ptr(T* ptr = nullptr)
		:m_ptr(ptr)
	{
		std::cout << "Auto_ptr parameterized ctor\n";
	}
 
	~Auto_ptr()
	{
		std::cout << "Auto_ptr destructor\n";
		delete m_ptr;
	}
 
	// Copy constructor
	// Do deep copy of a.m_ptr to m_ptr
	Auto_ptr(const Auto_ptr& a)
	{
		std::cout << "Auto_ptr Copy Constructor\n";
		m_ptr = new T;
		*m_ptr = *a.m_ptr;
	}
 
	// Move constructor
	// Transfer ownership of a.m_mptr to m_ptr
	Auto_ptr(Auto_ptr&& a)
		: m_ptr(a.m_ptr)
	{
		std::cout << "Auto_ptr Move Constructor\n";
		a.m_ptr = nullptr;
	}
 
	// Copy assignment
	// Do deep copy of a.m_ptr to m_ptr
	Auto_ptr& operator=(const Auto_ptr& a)
	{
		std::cout << "Auto_ptr Copy Assignment\n";
		// Self-assignment detection
		if (&a != this) {
			// Release any resource we're holding
			delete m_ptr;
	
			// Copy the resource
			m_ptr = new T;
			*m_ptr = *a.m_ptr;
		}
 
		return *this;
	}
 
	// Move assignment
	// Transfer ownership of a.m_ptr to m_ptr
	Auto_ptr& operator=(Auto_ptr&& a)
	{
		std::cout << "Auto_ptr Move Assignment\n";
		// Self-assignment detection
		if (&a == this)
			return *this;
 
		// Release any resource we're holding
		delete m_ptr;
 
		// Transfer ownership of a.m_ptr to m_ptr
		m_ptr = a.m_ptr;
		a.m_ptr = nullptr;
 
		return *this;
	}
 
	T& operator*() const { return *m_ptr; }
	T* operator->() const { return m_ptr; }
	bool isNull() const { return m_ptr == nullptr; }
};
 
class Resource
{
public:
	Resource() { std::cout << "Resource acquired\n"; }
	~Resource() { std::cout << "Resource destroyed\n"; }
};
 
Auto_ptr<Resource> generateResource()
{
	Auto_ptr<Resource> res(new Resource);
	return res; // this return value will invoke the move constructor
}
 
int main()
{
	std::cout << "*** In main()\n";
	Auto_ptr<Resource> mainres;
	std::cout << "*** Leaving main()\n";
	mainres = generateResource();	// this will invoke the move assignment operator
	std::cout << "*** Back in main()\n";
	std::cout << "*** End program\n";

	return 0;
}

/*
 * Running this code shows that the program runs much more efficiently. Only one resource is acquired, and only one 
 * resource is destroyed, which is really all we wanted from the code. 
 */