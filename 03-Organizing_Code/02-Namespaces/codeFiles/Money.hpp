#ifndef MONEY_HPP
#define MONEY_HPP

#include <ostream>

namespace MoneyMoneyMoney {
    class Money {
    public:
        Money();
        Money(int d, int c);
        friend const Money operator+(const Money& lhs, const Money& rhs);       // Addition is easier with pennies
        friend std::ostream& operator<<(std::ostream& out, const Money& rhs);
    private:
        int dollars;
        int cents;
        // But I have no helper function to get pennies listed
    };
}   // semicolon not required

#endif