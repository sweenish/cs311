#include "Money.hpp"

// unnamed namespace used to declare function at top of file
namespace {
    int getPennies(int d, int c);
}

namespace MoneyMoneyMoney {
    Money::Money() : dollars(0), cents(0)
    {
    }

    Money::Money(int d, int c) : dollars(d), cents(c)
    {
    }

    const Money operator+(const Money& lhs, const Money& rhs)
    {
        int lPennies = getPennies(lhs.dollars, lhs.cents);      // Function called and used here
        int rPennies = getPennies(rhs.dollars, rhs.cents);      // 
        int sumPennies = lPennies + rPennies;

        return Money(sumPennies / 100, sumPennies % 100);
    }

    // Only works for positive money values
    std::ostream& operator<<(std::ostream& out, const Money& rhs)
    {
        out << "$" << rhs.dollars << ".";
        if (rhs.cents < 10)
            out << "0";
        out << rhs.cents;

        return out;
    }
}

/*
 * unnamed namespace used and function implemented here
 * REMEMBER: The scope of the unnamed namespace is strictly to a compilation unit
 * I could not declare the function in the unnamed namespace of the header, and implement it here
 * Those two namespaces do not see each other at all
 */
namespace {
    /*
     * Utilizing the unnamed namespace allows me to hide a low-level helper function
     * It does not appear in the header, and it cannot be called outside of this file
     */
    int getPennies(int d, int c)
    {
        return ((d * 100) + c);
    }
}