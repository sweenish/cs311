#ifndef MONEY_HPP
#define MONEY_HPP

// namespace MyAwesomeBusiness {
	class Money {
	public:
		Money();
		Money(int d, int c);
		Money(int d);
		Money(double m);
		int getPennies() const;
		bool isNegative() const;
		void add(const Money &m);
		void subtract(const Money &m);
		bool isEqual(const Money &m) const;
		void print() const;
		friend std::ostream& operator<<(std::ostream& out, const Money& rhs);
	private:
		int dollars;
		int cents;
		// Helper functions
		int makePennies() const;	// EXTRA
		void validate();			// EXTRA
	};
// }

#endif