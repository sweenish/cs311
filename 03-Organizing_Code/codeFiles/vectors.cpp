#include <iostream>
#include <vector>
#include "Money.hpp"

// namespace MAB = MyAwesomeBusiness;

int main()
{
    std::vector<Money> v;

    for (int i = 0; i < 10; i++) {
        // v.push_back(3.50);
        // Money lhs(4.25), sum;
        // sum = lhs + 5;
        v.emplace_back(3.50);
        std::cout << "Value: " << v.at(i) << "\t\tSize: " << v.size() << "\t\tCapacity: " << v.capacity() << std::endl;
    }

    // for (int i : v)
    //     std::cout << i << std::endl;



    return 0;
}