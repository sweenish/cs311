#include <iostream>
#include <vector>


// This is my interface for all objects, no matter how they are augmented
class I {
public:
    virtual ~I(){}
    virtual void do_it() = 0;
};

// A was my base class, so it inherits directly from I
class A: public I {
public:
    ~A()
    {
        std::cout << "A dtor" << '\n';
    }
    /*virtual*/
    void do_it()
    {
        std::cout << 'A';
    }
};

// Decorator class that will be responsible for augmenting our base objects, aka our wrapper
class D: public I {
public:
    D(I *inner)
    {
        m_wrappee = inner;
    }
    ~D()
    {
        delete m_wrappee;
    }
    /*virtual*/
    void do_it()
    {
        m_wrappee->do_it();
    }
private:
    I *m_wrappee;
};

// My augments inherit from the decorator D, not I
// This I* core is what allows us to add multiple augments to our object dynamically
class X: public D {
public:
    X(I *core): D(core) {}
    ~X()
    {
        std::cout << "X dtor" << "   ";
    }
    /*virtual*/
    void do_it()
    {
        D::do_it();
        std::cout << 'X';
    }
};

class Y: public D {
public:
    Y(I *core): D(core) {}
    ~Y()
    {
        std::cout << "Y dtor" << "   ";
    }
    /*virtual*/
    void do_it()
    {
        D::do_it();
        std::cout << 'Y';
    }
};

class Z: public D {
public:
    Z(I *core): D(core) {}
    ~Z()
    {
        std::cout << "Z dtor" << "   ";
    }
    /*virtual*/
    void do_it()
    {
        D::do_it();
        std::cout << 'Z';
    }
};

int main()
{ 
    /*
     * As the 'new' commands continue to nest, we should see how they keep "wrapping" an object in a new layer
     * The line I *anX first calls for a new X, which takes as its parameter a new A. So, the A object is allocated 
     * first, and that address is passed to the X constructor. The X constructor simply passes the pointer along to the 
     * D constructor, which keeps track of it under the name wrapee. So, we end up with an X wrapping an A. 
     * 
     * When the do_it() function is called, we have to unwrap our classes until we get to executable code
     * For example:
     * anX->do_it();
     *  - anx is an I*, pointing to an X
     *  - So, the X class do_it() is called first
     *  - The first thing that function does is call the D::do_it()
     *  - The D object is holding an A object
     *  - The line wrapee->do_it() is executed, and that means that A->do_it() is executed
     *  - This prints the A, and we finally come back to the X->do_it(), where the X is then printed
     * 
     * This seems like a handful, especially when looking at those nested new statements when we construct an object, 
     * but the fact is that I have still simplified my overall inheritance structure, and I can augment whatever I want 
     * to an A, without having to inherit all possible combinations first.
     * 
     * And because the augmentation is dynamic, I can also put all these differently augmented versions into a vector 
     * if I wanted.
     */
    // I *anX = new X(new A);
    // I *anXY = new Y(new X(new A));
    // I *anXYZ = new Z(new Y(new X(new A)));
    // anX->do_it();
    // std::cout << '\n';
    // anXY->do_it();
    // std::cout << '\n';
    // anXYZ->do_it();
    // std::cout << '\n';
    // delete anX;
    // delete anXY;
    // delete anXYZ;

    /*
     * Comment out one block xor the other, and you'll see that they behave the same
     * The block below that utilizes a vector is impossible in the decorator_before code.
     */

    std::vector<I*> pointers;
    pointers.push_back(new X(new A));
    pointers.push_back(new Y(new X(new A)));
    pointers.push_back(new Z(new Y(new X(new A))));

    for(auto i : pointers) {
        i->do_it();
        std::cout << '\n';
    }

    for (auto &i : pointers)
        delete i;

    return 0;
}