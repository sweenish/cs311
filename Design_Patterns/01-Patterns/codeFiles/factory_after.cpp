#include <iostream>
#include <vector>

class Stooge {
public:
    // Factory Method - This function takes responsibility for object construction, it's the "factory"
    static Stooge *make_stooge(int choice);
    virtual void slap_stick() = 0;

    virtual ~Stooge() {}
};

int main()
{
    std::vector<Stooge*> roles;
    int choice;

    while (true)
    {
        std::cout << "Larry(1) Moe(2) Curly(3) Go(0): ";
        std::cin >> choice;
        if (choice == 0)
            break;
        roles.push_back(Stooge::make_stooge(choice));
    }

    for (unsigned int i = 0; i < roles.size(); i++)
        roles[i]->slap_stick();
    for (unsigned int i = 0; i < roles.size(); i++)
        delete roles[i];

    return 0;
}

// The classes themselves are not changed
class Larry: public Stooge {
public:
    void slap_stick() override
    {
        std::cout << "Larry: poke eyes\n";
    }

    ~Larry() {}
};

class Moe: public Stooge {
public:
    void slap_stick() override
    {
        std::cout << "Moe: slap head\n";
    }

    ~Moe() {}
};

class Curly: public Stooge {
public:
    void slap_stick() override
    {
        std::cout << "Curly: suffer abuse\n";
    }

    ~Curly() {}
};
/*
 * Whatever our reason was, we didn't want the programmer to be responsible for object construction
 * This function allows the programmer to provide a specification (a number choice in this case), and they receive 
 * the appropriate object in return
 */
Stooge* Stooge::make_stooge(int choice)
{
    if (choice == 1)
        return new Larry;
    else if (choice == 2)
        return new Moe;
    else
        return new Curly;
}