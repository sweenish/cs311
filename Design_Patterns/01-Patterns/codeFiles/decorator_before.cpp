#include <iostream>

class A {
public:
    virtual void do_it()
    {
        std::cout << 'A';
    }
};

// This looks fine. I want to augment A, so I derive from it and add extra attributes
class AwithX: public A {
public:
    /*virtual*/
    void do_it()
    {
        A::do_it();
        do_X();
    };
private:
    void do_X()
    {
        std::cout << 'X';
    }
};

class AwithY: public A {
public:
    /*virtual*/
    void do_it()
    {
        A::do_it();
        do_Y();
    }
protected:
    void do_Y()
    {
        std::cout << 'Y';
    }
};

class AwithZ: public A {
public:
    /*virtual*/
    void do_it()
    {
        A::do_it();
        do_Z();
    }
protected:
    void do_Z()
    {
        std::cout << 'Z';
    }
};

// At this point, the amount of inheritance is starting to get a little much
// Every combination of augments are requiring their own derivation; it's a lot to keep track of
class AwithXY: public AwithX, public AwithY
{
public:
    /*virtual*/
    void do_it()
    {
        AwithX::do_it();
        AwithY::do_Y();
    }
};

class AwithXYZ: public AwithX, public AwithY, public AwithZ
{
public:
    /*virtual*/
    void do_it()
    {
        AwithX::do_it();
        AwithY::do_Y();
        AwithZ::do_Z();
    }
};

int main()
{
    AwithX anX;         // Note that each type will be augmented the way I want
    AwithXY anXY;       // But they are also different types; this is a maintenance nightmare
    AwithXYZ anXYZ;
    anX.do_it();
    std::cout << '\n';
    anXY.do_it();
    std::cout << '\n';
    anXYZ.do_it();
    std::cout << '\n';
}