#include <iostream>

class Outer {
public:
    Outer();
    int getOuterData() const;
    int getInnerData() const;
    void setStatic(int n);
    static int getSharedOuterData();
private:
    int outerData;
    static int sharedOuterData;

    class Inner {   // Nested class, but still just a declaration
    public:
        Inner();
        int getInnerData() const;
        // void setOuterData(int newValue);     // Not possible unless data is static
        void setOuterStaticData(int newValue);
    private:
        int innerData;
    };  // End declaration of nested class

    Inner innerObj; // Still need an actual object or pointer to nested object if we intend to use it
};  // End declaration of enclosing class

// Static Initialization
int Outer::sharedOuterData = 8;

int main()
{
    Outer out;

    std::cout << "Outer Data: " << out.getOuterData() << std::endl;
    std::cout << "Inner Data: " << out.getInnerData() << std::endl;
    std::cout << "Static Data: " << Outer::getSharedOuterData() << std::endl;
    out.setStatic(56);
    std::cout << "Static Data: " << Outer::getSharedOuterData() << std::endl;

    return 0;
}

Outer::Outer() : outerData(42), innerObj(Inner()) { }

int Outer::getOuterData() const
{
    return outerData;
}

int Outer::getInnerData() const
{
    return innerObj.getInnerData();
    // return innerObj.innerData; // "identifier innerData is undefined"
}

void Outer::setStatic(int n)
{
    innerObj.setOuterStaticData(n);
}

int Outer::getSharedOuterData()
{
    return Outer::sharedOuterData;
}

Outer::Inner::Inner() : innerData(34) { }

int Outer::Inner::getInnerData() const
{
    return innerData;
}

void Outer::Inner::setOuterStaticData(int newValue)
{
    sharedOuterData = newValue;
}