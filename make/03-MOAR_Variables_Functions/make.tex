\documentclass[10pt]{beamer}
\usetheme{metropolis}

\usepackage{minted}
\usepackage{textcomp}

\title{\texttt{make}}
\author{Adam Sweeney}
\subject{CS 311}
\institute{Wichita State University}

\begin{document} % TITLE
\begin{frame}
	\titlepage
\end{frame}

\AtBeginSection[]{ %
	\begin{frame}
		\vfill{}
		\begin{center}
			\textbf{\LARGE{\insertsectionhead}}
		\end{center}
		\vfill{}
	\end{frame}
}

\begin{frame}
	\frametitle{Introduction}
	\begin{itemize}
		\item From the GNU \texttt{make} manual\\
		\textit{``The make utility automatically determines pieces of a large program that need
		to be recompiled, and issues commands to recompile them.''}
		\vspace{1em}
		\item Compiling our programs will be much easier with \texttt{make}
		\item Just cover basics\ldots{}for now
	\end{itemize}
\end{frame}

\section{Part One\\\vspace{1em}Introduction \& Basics}
\begin{frame}
	\frametitle{What is \texttt{make}?}
	\begin{itemize}
		\item \texttt{make} is a utility
		\item It requires a file called makefile in the current working directory
		\item At its most basic, a makefile consists of rules that \texttt{make} follows
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{What a rule looks like}
	\begin{minted}[gobble=2, tabsize=4, linenos]{make}
		target: pre-requisites
			Recipe
			.
			.
	\end{minted}
	\begin{itemize}
		\item \texttt{target} - typically the name of output file generated\\
		\item \texttt{pre-requisites} - inputs required to create a target\\
		\item \texttt{recipe} - action(s) that \texttt{make} carries out
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{IMPORTANT}
	\begin{itemize}
		\item Every recipe line \textbf{MUST} begin with a tab\\
		\item Not 4 spaces, an actual tab character
		\begin{itemize}
			\item If using a ``modern'' text editor, simply be sure to name your file before you write anything
		\end{itemize}
	\end{itemize}
	
\end{frame}

\begin{frame}[fragile]
	\frametitle{Super-Simple makefile}
	\begin{minted}[gobble=2, tabsize=4, linenos]{make}
		hotdog: Hotdog.cpp
			g++ -Wall -o hotdog Hotdog.cpp

		clean:
			rm -f core hotdog
	\end{minted}
\end{frame}

\begin{frame}
	\frametitle{Using the makefile}
	\begin{itemize}
		\item Ensure the file \texttt{makefile} is located in the same directory as the source
		\item Run the command "\texttt{make}"
		\item To delete everything created by the makefile, run the command \texttt{make clean}
		\item \texttt{clean} is not a file, but just the name of an action
		\item It is not a pre-requisite, nor does it have any of its own
		\item It just runs its recipe only when it is specifically called
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{How \texttt{make} Processes a makefile}
	\begin{itemize}
		\item Assuming \texttt{make} was invoked without specifying a target, it starts at the top
		\item Before the first rule can be fully processed, it must process the rules of
		the pre-requisites
		\item Uses timestamps to re-compile/re-link only what has changed from the last compile
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{\texttt{make} and Variables}
	\begin{itemize}
		\item Variables make makefiles simpler
		\item Reduce duplication, thereby reducing opportunities for error
	\end{itemize}
\end{frame}

\section{Part Two\\\vspace{1em}Variables \& Built-in Rules}
\begin{frame}
	\frametitle{Variables}
	\begin{itemize}
		\item \texttt{make} is able to use variables
		\item This reduces repititive typing and opportunities to make mistakes
		\item They look a little different than what we're used to in C++
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{A makefile without Variables}
	\begin{minted}[gobble=2, tabsize=4, linenos, xleftmargin=2mm]{make}
		hotdog: Hotdog.cpp
			g++ -Wall -o hotdog Hotdog.cpp

		clean:
			rm -f core hotdog
	\end{minted}
\end{frame}

\begin{frame}[fragile]
	\frametitle{A makefile with Variables}
	\begin{minted}[gobble=2, tabsize=4, linenos, xleftmargin=2mm]{make}
		CXX = g++
		CXXFLAGS = -Wall
		SRC = Hotdog.cpp
		EXE = hotdog

		$(EXE): $(SRC)
			$(CXX) $(CXXFLAGS) -o $(EXE) $(SRC)

		clean:
			rm -f core hotdog
	\end{minted}
\end{frame}

\begin{frame}
	\frametitle{Built-in Variables}
	\begin{itemize}
		\item \texttt{make} provides some variables by default, called implicit variables
		\item \texttt{CXX} \& \texttt{CXXFLAGS} are two of those
		\begin{itemize}
			\item There is also an implicit variable \texttt{CPPFLAGS}
			\item It is intended for the C preprocessor, and not the C++ compiler
		\end{itemize}
		\item \texttt{CXX} has a default value (g++), but \texttt{CXXFLAGS} does not
		\item These exist to provide a standard interface to a makefile
		\item The others used (\texttt{SRC} \& \texttt{EXE}) are not implicit, but widely used
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{General Rules of Thumb for Variables}
	\begin{itemize}
		\item Use implicit variables where they make sense
		\item Use all caps for variable names
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Phony Targets}
	\begin{itemize}
		\item Not all rules in a makefile build a target; they exist simply to execute some actions for us
		\item The \texttt{clean} rule is a prime example
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Special Built-in Target Names}
	\begin{itemize}
		\item In the same way that \texttt{make} provides some variables names by default, it also provides some 
		targets
		\item The only one we will consider is \texttt{.PHONY}
		\item From the documentation:\\
		``A phony target is one that is not really the name of a file; rather it is just a name for a recipe to be 
		executed when you make an explicit request.''
		\item The \texttt{.PHONY} target helps us avoid file conflicts, and can improve performance
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Our \texttt{makefile} Now}
	\begin{minted}[gobble=2, tabsize=4, linenos, fontsize=\small]{make}
		CXX = g++
		CXXFLAGS = -Wall
		SRC = Hotdog.cpp
		EXE = hotdog
	
		$(EXE): $(SRC)
			$(CXX) $(CXXFLAGS) -o $(EXE) $(SRC)
		
		.PHONY: clean
		clean:
			rm -f core hotdog
	\end{minted}
\end{frame}

\begin{frame}
	\frametitle{Avoiding File Collisions}
	\begin{itemize}
		\item As a target, \texttt{clean} has no pre-requisites
		\item Also, because the recipe does not create a file called \texttt{clean}, the recipe will generally always 
		execute when invoked
		\item UNLESS we have a file called clean in the directory
		\item Then, the fact that there are no pre-requisites becomes a detriment
		\begin{itemize}
			\item There is nothing to check, so the file is assumed to be always up-to-date
			\item This means the recipe would never execute
		\end{itemize}
		\item \texttt{.PHONY} will ensure that its pre-requisites always execute their recipes
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Continuing}
	\begin{itemize}
		\item There is a lot more to \texttt{make} than what was shown here
		\vspace{1em}
		\item A lot
		\item The GNU documentation is a great source of further information
		\item The information in this presentation should help you understand the basics
	\end{itemize}
\end{frame}

\section{MOAR Variables \& A Function}
\begin{frame}
	\frametitle{But First, Comments}
	\begin{itemize}
		\item \texttt{make} only has one type of comment
		\item Wherever you place `\texttt{\#}', the remainder of that \textit{line} is considered a comment
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{More About Variables}
	\begin{itemize}
		\item \texttt{make} has two general types of variables, called flavors
		\item The first is what we've been using all along so far
		\item These are called recursively expanded variables
		\begin{itemize}
			\item The value you specify is stored verbatim
			\item If it contains references to variables, the references are expanded \textit{whenever the variable 
			is substituted}
			\item When that happens, it's called recursive expansion
		\end{itemize}
		\item Pros
			\begin{itemize}
				\item Expansion is automatic and on demand
			\end{itemize}
		\item Cons
			\begin{itemize}
				\item Appending is impossible
				\item Functions are executed on every expansion, usually causing unpredictable results
			\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{The Other Flavor}
	\begin{itemize}
		\item The other flavor of variables are called simply expanded variables
		\item Defined using `:=' or `::=' (We will prefer `:=')
		\item The value of a simply expanded variable is scanned once and for all, expanding any references only when 
		the variable is defined
		\item In other words, it does \textit{not} contain references to other variables; it contains their 
		\textit{values as of the time the variable was defined}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{So, I Wrote Out All My Source Files}
	\begin{itemize}
		\item Do I really have to write out the same thing, just with .o at the end?
		\begin{itemize}
			\item No, you do not
		\end{itemize}
		\item We can have \texttt{make} do it for us
		\item This is called substitution reference
		\item Substitution references substitute a variable with alterations that we specify
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{For Example}
	\begin{minted}[gobble=2, tabsize=4, linenos, xleftmargin=4em]{make}
		SRCS = Foo.cpp main.cpp
		OBJS = $(SRCS:%.cpp=%.o)
	\end{minted}
	\begin{itemize}
		\item \texttt{OBJS} is equal to \texttt{Foo.o main.o}
		\item The \texttt{\%} is a wildcard, equivalent to the * in the terminal
		\begin{itemize}
			\item While not required, we will prefer to use the \texttt{\%}
			\item It helps make things a bit clearer to understand
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{A Function}
	\begin{itemize}
		\item We're learning that \texttt{make} has a lot of stuff built-in to help us out
		\item There are built-in variables, targets, and also functions
		\item We will look at the function \texttt{addprefix}
		\item As its name implies, it can be used to add a prefix to a string or variable
		\item Using it looks like this:
		\begin{minted}[gobble=3, tabsize=4, linenos, xleftmargin=4em]{make}
			$(addprefix PREFIX, NAME)
		\end{minted}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]
	\frametitle{More Exampling}
	\begin{minted}[gobble=2, tabsize=4, linenos, xleftmargin=4em]{make}
		SRCS = Foo.cpp main.cpp
		SRCDIR = src
		OBJS = $(SRCS:%.cpp=%.o)
		OBJS := $(addprefix $(SRCDIR)/, $(OBJS))
	\end{minted}
	\begin{itemize}
		\item \texttt{OBJS} is now equal to \texttt{src/Foo.o src/main.o}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{We're Not Done with Variables Yet}
	\begin{itemize}
		\item We'll learn a bit about automatic variables
		\item Once we figure these out, our makefiles actually become easier to write
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{How Automatic Variables Help Us}
	\begin{itemize}
		\item As our programs grew, so did our makefiles
		\item Being forced to change a filename can become quite tedious
		\item Previous use of variables only worked on one target, or were worthless
		\item There is a better way: automatic variables
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Presenting\ldots{}}
	\begin{itemize}
		\item \$$@$ : The file name of the target of the rule\\
		\textit{In a pattern with multiple targets, \$$@$ is the name of whichever target triggered the recipe}
		\item \$$<$ : the name of the first pre-requisite
		\item \$\textasciicircum{} : The names of \textit{all} the pre-requisites with a space between them
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{This is Supposed to Make My Life Easier\ldots{}How?}
	\begin{itemize}
		\item Automatic variables allow us to define a \textit{single} rule that will compile every *.o file for us automatically
		\item Still respects when classes don't need to be re-compiled
	\end{itemize}
\end{frame}

\end{document}
