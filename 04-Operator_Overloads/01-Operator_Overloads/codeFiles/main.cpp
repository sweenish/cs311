#include <iostream>
#include "Pair.hpp"
#include "shortcut.hpp"

int main()
{
    WS::Pair alpha(34, 'B');
    WS::Pair beta(78, 'c');
    WS::Pair result;

    /*
     * Rules for addition:
     * The integer part is the sum of the value of the character on the right-hand side is added to the 
     * integer value on the left side
     * The character is replaced by an upper case letter corresponding to the integer on the right hand 
     * side, mod 26
     * Example:
     *      (34, B) + (78, c) = ((34 + 99(c)), 78 % 26 -> upper case letter) -> (133, A)
     */
    //result = alpha + beta;
    std::cout << (alpha + beta);
    std::cout << std::endl;

    /*
     * Rules for subtraction
     * The integer part is the difference between the left operand's integer and the integer value of the 
     * right-hand side's character
     * The character part is replaced by a lower case letter corresponding to the integer on the left hand 
     * side, mod 26
     * Example:
     *  (34, 8) - (78, c) = ((34 - 99(c)), 34 % 26 -> lower case letter) -> (-65, i)
     */

    std::cout << (alpha - beta);
    std::cout << std::endl;


    return 0;
}