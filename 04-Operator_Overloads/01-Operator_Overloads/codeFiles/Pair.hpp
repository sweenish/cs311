#ifndef PAIR_HPP
#define PAIR_HPP

namespace weirdStuff {
    class Pair {
    public:
        Pair();
        Pair(int f, char s);
        int getFirst() const;
        char getSecond() const;
        // void print() const;
        // Addition Overload as a member function
        const Pair operator+(const Pair &rhs) const;
        // Insertion Overload as a friend function
        friend std::ostream& operator<<(std::ostream& outS, const Pair& rhs);
    private:
        int first;
        char second;
    };


// Subtraction overload as a non-member function
const Pair operator-(const Pair &lhs, const Pair &rhs);

};

#endif