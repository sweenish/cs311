#include <iostream>
#include <string>
#include "Employee.hpp"

namespace companyEmployees
{
	Employee::Employee() : name("None Entered"), ssn("None Entered"), netPay(0)
	{ }

	Employee::Employee(const std::string& name, const std::string& ssn) : 
							name(name), ssn(ssn), netPay(0)
	{ }

	std::string Employee::getName() const { return name; }

	std::string Employee::getSSN() const { return ssn; }

	double Employee::getNetPay() const { return netPay; }

	void Employee::setName(const std::string& newName)
	{
		name = newName;
	}

	void Employee::setSSN(const std::string& newSSN)
	{
		ssn = newSSN;
	}

	void Employee::setNetPay(const double newPay)
	{
		netPay = newPay;
	}

	void Employee::printCheck() const
	{
		std::cout << "\nError: printCheck FUNCTION CALLED FOR AN\n"
				<< "UNDIFFERENTIATED EMPLOYEE.\n";
		return;
	}
}
