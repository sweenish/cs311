#ifndef EMPLOYEE_HPP
#define EMPLOYEE_HPP

#include <string>

namespace companyEmployees
{
	class Employee {
	public:
		Employee();
		Employee(const std::string& name, const std::string& ssn);
		std::string getName() const;
		std::string getSSN() const;
		double getNetPay() const;
		void setName(const std::string& newName);
		void setSSN(const std::string& newSSN);
		void setNetPay(const double newPay);
		void printCheck() const;
	private:
		std::string name;
		std::string ssn;
		double netPay;
	};
}

#endif