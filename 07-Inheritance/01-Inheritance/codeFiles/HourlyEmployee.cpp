#include <iostream>
#include <string>
#include "HourlyEmployee.hpp"

namespace companyEmployees
{
	HourlyEmployee::HourlyEmployee() : Employee(), wageRate(0), hours(0)
	{ }

	HourlyEmployee::HourlyEmployee(const std::string name, const std::string ssn, 
		double rate, double hours) : Employee(name, ssn), wageRate(rate), hours(hours)
	{ }

	double HourlyEmployee::getRate() const { return wageRate; }

	double HourlyEmployee::getHours() const { return hours; }

	void HourlyEmployee::setRate(const double newRate)
	{
		wageRate = newRate;
	}

	void HourlyEmployee::setHours(const double newHours)
	{
		hours = newHours;
	}

	void HourlyEmployee::printCheck()
	{
		setNetPay(hours * wageRate);

		std::cout << "\n_____________________________________________________\n";
		std::cout << "Pay to the order of " << getName() << std::endl;
		std::cout << "The sum of " << getNetPay() << " Dollars" << std::endl;
		std::cout << "_____________________________________________________\n";
		std::cout << "Check stub: NOT NEGOTIABLE\n";
		std::cout << "Employee number: " << getSSN() << std::endl;
		std::cout << "Hourly Employee.\nHours Worked: " << hours << std::endl;
		std::cout << "Rate: " << wageRate << " Pay: " << getNetPay() << std::endl;
		std::cout << "_____________________________________________________\n";
	}
}