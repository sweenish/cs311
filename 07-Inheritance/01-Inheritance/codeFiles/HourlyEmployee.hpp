#ifndef HOURLY_EMPL_HPP
#define HOURLY_EMPL_HPP

#include <string>
#include "Employee.hpp"

namespace companyEmployees
{
	class HourlyEmployee : public Employee {
	public:
		HourlyEmployee();
		HourlyEmployee(const std::string name, const std::string ssn, double rate, double hours);
		double getRate() const;
		double getHours() const;
		void setRate(const double newRate);
		void setHours(const double newHours);
		void printCheck();
	private:
		double wageRate;
		double hours;
	};
}

#endif