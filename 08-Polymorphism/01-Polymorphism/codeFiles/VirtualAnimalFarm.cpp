#include <iostream>
#include <string>
#include <vector>

class Animal {
protected:
	std::string name;
	Animal(std::string name) : name(name) { }
public:
	std::string getName() const { return name; }
	virtual const char* speak() const = 0;	// Pure virtual function, Animal is now an abstract class
	/*
	 * Virtual functions allow us to get around the issue we had in AnimalFarm.cpp
	 * By implementing late/dynamic binding, we are able to have a POINTER or REFERENCE to a base class
	 * invoke the derived version of a function
	*/
};

class Cat : public Animal {
public:
	Cat(std::string name) : Animal(name) { }
	const char* speak() const override { return "Meow"; }
};

class Kitten : public Cat {
public:
	Kitten(std::string name) : Cat(name) { }
	const char* speak() const override final { return "Squeek"; }
};

class Dog : public Animal {
public:
	Dog(std::string name) : Animal(name) { }
	const char* speak() const override final { return "Woof"; }
};

void report(const Animal& animal)
{
	std::cout << animal.getName() << " says " << animal.speak() << "\n";
}

int main()
{
	// By making the vector of type Animal*, each element is able to point to any Animal-derived object
	std::vector<Animal*> pets;
	pets.push_back(new Dog("Dog"));
	pets.push_back(new Cat("Purrscilla"));
	pets.push_back(new Kitten("Sir Cuddles"));
	// Dynamic binding and dynamic memory; they go together like peas and carrots

	// This is a range-based for loop. If your linter is working well, hovering over i should show its type
	for (auto i : pets)
		report(*i);

	/* Also a valid loop [NOTE: i has a different type for this loop] */
	for (unsigned int i = 0; i < pets.size(); i++)
		report(*(pets.at(i)));

	Dog dog("Roofus");
	Animal *ani_ptr = &dog;	// My base class pointer can point to statically declared derived objects
	report(*ani_ptr);

	report(dog);

	return 0;
}
