#include <iostream>
#include <string>

// Base class
class Animal {
public:
	std::string getName() const { return name; }
	const char* speak() const { return "???"; }
protected:
	std::string name;
	Animal(std::string name) : name(name) { }
};

class Cat : public Animal {
public:
	Cat(std::string name) : Animal(name) { }
	const char* speak() const { return "Meow"; }
};

class Dog : public Animal {
public:
	Dog(std::string name) : Animal(name) { }
	const char* speak() const { return "Woof"; }
};

/*
 * We have many different derived Animals, but we can say that each one "is a" Animal
 * We only want to write a single function that will allow any object derived from animal to report
 * based on ITS speak() function 
*/
void report(const Animal& animal)
{
	std::cout << animal.getName() << " says " << animal.speak() << "\n";
}

int main()
{
	Cat cat("Priscilla");
	Dog dog("Dog");

	// When we run the program, we find out that the base class speak() is called
	// The "is a" relationship held true; the code compiled
	// But it called the base version of speak(), and not the proper derived version
	report(cat);
	report(dog);
}