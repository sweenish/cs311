#include <iostream>
#include <string>

// Base class
class Pet {
public:
	std::string name;
	virtual void print() const;	// Try making this function pure virtual and understanding why certain declaration do or don't break
};

class Dog : public Pet {
public:
	std::string breed;
	void print() const override;
};

class Cat : public Pet {
public:
	std::string breed;
	std::string personality;
	void print() const override;
};

int main()
{
	Dog vdog;
	Pet vpet;
	vdog.name = "Tiny";
	vdog.breed = "Great Dane";
	vpet = vdog;	// Legal, a dog 'is a' pet
	std::cout << "The slicing problem:\n"; 	// Assigning a derived object to a static base class object will slice it.
	vpet.print();							// Even though print() is virtual, it thinks our most derived version is 
											// the base class version, this is because virtual does its thing at 
											// runtime, but we've forced everything to be determined at compile time
	std::cout << "Note that it was print from Pet that was invoked.\n\n";

	std::cout << "The slicing problem defeated:\n";
	Pet *ppet;
	Dog *pdog;
	Cat *pcat;
	pdog = new Dog;
	pdog->name = "Tiny";	// (*pdog).name = "Tiny";
	pdog->breed = "Great Dane";
	ppet = pdog;	// Legal, a dog 'is a' pet
	ppet->print();	// But now, our virtual functions work as expected since we are using pointers to the base class

	pcat = new Cat;
	pcat->name = "Dame Fluff";
	pcat->breed = "Russian Blue";
	pcat->personality = "Entitled";
	ppet = pcat;
	ppet->print();

	/*
	 * NOTE: This statement would still error out if ppet is of type Animal*
	 * std::cout << "Name: " << ppet->name << " breed: " << ppet->breed << std::endl;
	 * The class Pet still doesn't have a member variable called breed
	 */

	 return 0;
}
void Pet::print() const
{
	std::cout << "Name: " << name << std::endl;
}

void Dog::print() const
{
	std::cout << "Name: " << name << std::endl;
	std::cout << "Breed: " << breed << std::endl;
}

void Cat::print() const
{
	std::cout << "Name: " << name << std::endl;
	std::cout << "Breed: " << breed << std::endl;
	std::cout << "Personality: " << personality << std::endl;
}