#include <iostream>

int main()
{
	int donuts, milk;
	double dpg;

	try {	// We place our algorithm in a try block, this is where the normal happens
		std::cout << "Enter a number of donuts: ";
		std::cin >> donuts;
		std::cout << "Enter number of glasses of milk: ";
		std::cin >> milk;

		if (milk <= 0)
			throw donuts;	// When something excpetional happens, we divert the flow of control

		dpg = static_cast<double>(donuts) / milk;
		std::cout << donuts << " donuts.\n" << milk << " glasses of milk.\n"
				<< "You have " << dpg << " donuts for each glass of milk.\n";
	}
	catch (int e) {	// The catch block is where we handle exceptional situations
		std::cout << e << " donuts and no milk :-(\nGo buy some milk.\n";
	}
	
	return 0;
}