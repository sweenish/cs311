#include <iostream>

// An exception class is a regular class that we happen to be using for exceptions
class NoMilk {
public:
	NoMilk() {}
	NoMilk(int howMany) : count(howMany) {}
	int getCount() const { return count; }
private:
	int count;
};

int main()
{
	int donuts, milk;
	double dpg;

	try {
		std::cout << "Enter a number of donuts: ";
		std::cin >> donuts;
		std::cout << "Enter number of glasses of milk: ";
		std::cin >> milk;

		if (milk <= 0)
			throw NoMilk(donuts);

		dpg = static_cast<double>(donuts) / milk;
		std::cout << donuts << " donuts.\n" << milk << " glasses of milk.\n"
				<< "You have " << dpg << " donuts for each glass of milk.\n";
	}
	catch (NoMilk e) {
		std::cout << e.getCount() << " donuts and no milk :-(\nGo buy some milk.\n";
	}
	return 0;
}