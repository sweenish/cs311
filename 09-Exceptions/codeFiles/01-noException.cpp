#include <iostream>

// Our toy example with no exceptions

int main()
{
	int donuts, milk;
	double dpg;

	std::cout << "Enter a number of donuts: ";
	std::cin >> donuts;
	std::cout << "Enter number of glasses of milk: ";
	std::cin >> milk;

	if (milk <= 0) {
		std::cout << donuts << " donuts and no milk :-(\nGo buy some milk.\n";
	} else {
		dpg = static_cast<double>(donuts) / milk;
		std::cout << donuts << " donuts.\n" << milk << " glasses of milk.\n"
				<< "You have " << dpg << " donuts for each glass of milk.\n";
	}

	return 0;
}