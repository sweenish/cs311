#include <iostream>
#include <string>

class NegativeNumber {  // An exception class
public:
    NegativeNumber() {}
    NegativeNumber(std::string theMessage) : message(theMessage) {}
    std::string getMessage() const { return message; }
private:
    std::string message;
};

class DivideByZero { }; // A trivial exception class

int main()
{
    int pencils, erasers;
    double ppe; // Pencils Per Eraser
    
    // A try block can contain many throw statements, but will only ever throw a single exception
    try {
        std::cout << "How many pencils do you have? ";
        std::cin >> pencils;
        if (pencils < 0)
            throw NegativeNumber("pencils");    // One throw, ah ah ah
            
        std::cout << "How many erasers do you have? ";
        std::cin >> erasers;
        if (erasers < 0)
            throw NegativeNumber("erasers");    // Two throw, ah ah ah
        
        if (erasers != 0)
            ppe = static_cast<double>(pencils) / erasers;
        else
            throw DivideByZero();               // Three throw, ah ah ah
        
        std::cout << "Each eraser must last through " << ppe << " pencils.\n";
    }
    /*
     * IMPORTANT: While these catch blocks are listed in order, that is not how to organize our catch blocks!
     * We catch the most specific exceptions first, then get more generic as we go down the catch blocks
     * In this case, the NegativeNumber and DivideByZero are equally important, so the order happens to 
     * match that of their appearances in the try block
     */
    catch(NegativeNumber e) {
        std::cout << "Cannot have a negative number of " << e.getMessage() << std::endl;
    }
    catch(DivideByZero) {
        std::cout << "Do not make any mistakes.\n";
    }
    catch(...) {
        std::cout << "We shouldn't be here.\n";
    }
    std::cout << "End of program.\n";

    return 0;
}