#include <cctype>
#include <iostream>
#include <random>
#include <string>
#include <utility>

const int MAX = 5;

template<class T>
void bubbleSort(T arr[], int size);

template<>
void bubbleSort<std::string>(std::string arr[], int size);

template<class T>
void printArray(T arr[], int size);

std::string capitalize(std::string word);

int main()
{
	int iarr[MAX];
	double darr[MAX];
	std::string sarr[MAX] = {"cat", "aardvark", "Zebra", "Mouse", "zebras"};
	std::random_device rando;
	std::mt19937 engine(rando());
	std::uniform_int_distribution<int> distro(1, 500);

	std::cout.setf(std::ios::fixed);
	std::cout.setf(std::ios::showpoint);
	std::cout.precision(3);

	for (int i = 0; i < MAX; ++i) {
		iarr[i] = distro(engine);
		darr[i] = distro(engine) / 13.0;
	}

	std::cout << "Integer list: ";
	printArray(iarr, MAX);
	std::cout << "Double list: ";
	printArray(darr, MAX);
	std::cout << "String list: ";
	printArray(sarr, MAX);

	bubbleSort(iarr, MAX);
	bubbleSort(darr, MAX);
	bubbleSort(sarr, MAX);

	std::cout << "\n\nSorted Integer list: ";
	printArray(iarr, MAX);
	std::cout << "Sorted Double list: ";
	printArray(darr, MAX);
	std::cout << "Sorted String list: ";
	printArray(sarr, MAX);

	return 0;
}

template<class T>
void printArray(T arr[], int size)
{
	for (int i = 0; i < MAX; ++i)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

template<class T>
void bubbleSort(T arr[], int size)
{
	bool swapped;
	do {
		swapped = false;
		for (int i = 1; i < size; ++i) {
			if (arr[i - 1] > arr[i]) {
				std::swap(arr[i - 1], arr[i]);
				swapped = true;
			}
		}
	} while(swapped == true);
}

template<>
void bubbleSort<std::string>(std::string arr[], int size)
{
	bool swapped;
	do {
		swapped = false;
		for (int i = 1; i < size; ++i) {
			if ( capitalize(arr[i - 1]) > capitalize(arr[i]) ) {
				std::swap(arr[i - 1], arr[i]);
				swapped = true;
			}
		}
	} while(swapped == true);
}

std::string capitalize(std::string word)
{
	for (unsigned int i = 0; i < word.length(); ++i)
		word.at(i) = toupper(word.at(i));
	
	return word;
}