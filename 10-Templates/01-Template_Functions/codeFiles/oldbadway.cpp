#include <iostream>

void swap(int& a, int& b);
void swap(double& a, double& b);

int main()
{
	int x = 88, y = 94;
	double p = 3.14, g0 = 7.748e-5;

	std::cout << "Integers: " << x << ", " << y << std::endl;
	std::cout << "Doubles: " << p << ", " << g0 << std::endl;
	swap(x, y);
	swap(p, g0);
	std::cout << "Integers: " << x << ", " << y << std::endl;
	std::cout << "Doubles: " << p << ", " << g0 << std::endl;

	// What if I need to swap other types? Should I really be expected to 
	// overload swap for every conceivable data type?
	return 0;
}

void swap(int& a, int& b)
{
	int temp;

	temp = a;
	a = b;
	b = temp;
}

// It's the same, except for the types, I've basically duplicated my code
void swap(double& a, double& b)
{
	double temp;

	temp = a;
	a = b;
	b = temp;
}