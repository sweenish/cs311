#include <iostream>

// A template function
template <typename T>
void show(T arr[], size_t size)
{
    for (size_t i = 0; i < size; i++)
        std::cout << arr[i] << " ";
}

// A full template specialization for the function show()
template <>
void show<char>(char arr[], size_t size)
{
    for (size_t i = 0; i < size; i++)
        std::cout << arr[i];
}

int main()
{
    int a[10];

    int num = 1;
    for (auto &i : a) {
        i = num;
        ++num;
    }

    show(a, 10);
    std::cout << "\n\n";

    char c[10] = "Hello";
    show(c, 10);
    std::cout << std::endl;

    return 0;
}

/*
 * In this program, we specialized the function if T happens to be char
 * Specialized versions of template functions will always have precedence over 
 * the generic version.
 * 
 * There is also no rule that a specialization has to behave the same (with 
 * slight tweaks). We can think of it as an overload, we can do whatever we want
 * in the function body. Generally, we keep it pretty close to the generic 
 * version so as to not subvert our expectations (TM).
 */