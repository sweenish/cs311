#include <iostream>

// A template function
template <typename T>
void show(T arr[], size_t size)
{
    for (size_t i = 0; i < size; i++)
        std::cout << arr[i] << " ";
}

int main()
{
    int a[10];

    int num = 1;
    for (auto &i : a) {
        i = num;
        ++num;
    }

    show(a, 10);
    std::cout << "\n\n";

    char c[10] = "Hello";
    show(c, 10);
    std::cout << std::endl;

    return 0;
}

/*
 * What we have here is a simple template function that prints the elements of 
 * an array to the screen. We see that works for two different basic types, 
 * but we would prefer that if the array is of type char, we don't put the 
 * extra spaces between characters. 
 */