#ifndef PAIR_HPP
#define PAIR_HPP

#include <iostream>

// Prep friend function template
template <typename T> class Pair;
template <typename T>
    std::ostream& operator <<(std::ostream& sout, const Pair<T> &rhs);


template <typename T>
class Pair {
public:
    Pair();
    Pair(T one, T two);
    friend std::ostream& operator << <T>(std::ostream& sout, 
                                         const Pair<T> &rhs);
private:
    T first;
    T second;
};


// Class implementation
template <typename T>
Pair<T>::Pair() : first(T()), second(T()) { }

template <typename T>
Pair<T>::Pair(T one, T two) : first(one), second(two) { }

template <typename T>
std::ostream& operator <<(std::ostream& sout, const Pair<T> &rhs)
{
    return sout << "(" << rhs.first << ", " << rhs.second << ")";
}


// If this next line is throwing you, just take the contents of the file being 
// included and paste them right over the include directive. That's what the 
// compiler will do.
#include "ptr_Pair.inl"

#endif

/*
 * Hopefully nothing in this file is a surprise to anyone.
 */