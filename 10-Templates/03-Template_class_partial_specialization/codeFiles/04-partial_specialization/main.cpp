#include <iostream>
#include "Pair.hpp"

int main()
{
    Pair<int> ipair(42, 55);
    Pair<int*> iptrpair(new int(37), new int(77));

    std::cout << ipair << std::endl;
    std::cout << iptrpair << std::endl;

    /*
     * The reasoning behind the partial specialization is because no matter 
     * what the type of Pair is, we want the << operator to work as expected
     * 
     * A partial specialization means that for no matter what pointer to T we 
     * assign to a pair, it will work. (Assuming that T has an overload for the 
     * << operator as well)
     * 
     * A real world example is std::unique_ptr. It is partially specialied for 
     * the case where it manages a pointer to an array. 
     */

    return 0;
}