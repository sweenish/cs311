#include <iostream>
#include "douPair.hpp"

DPair::DPair()
{ }

DPair::DPair(double a, double b) : first(a), second(b)
{ }

void DPair::print() const
{
	std::cout << "(" << first << ", " << second << ")" << std::endl;
}

void DPair::swap()
{
	double temp = first;
	first = second;
	second = temp;
}