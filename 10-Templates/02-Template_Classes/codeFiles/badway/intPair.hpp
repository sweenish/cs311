#ifndef IPAIRCLASS_HPP
#define IPAIRCLASS_HPP

class IPair {
public:
	IPair();
	IPair(int a, int b);
	void print() const;
	void swap();
private:
	int first = 0;
	int second = 0;
};

#endif