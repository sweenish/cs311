#ifndef DPAIRCLASS_HPP
#define DPAIRCLASS_HPP

class DPair {
public:
	DPair();
	DPair(double a, double b);
	void print() const;
	void swap();
private:
	double first = 0.0;
	double second = 0.0;
};

#endif