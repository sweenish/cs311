#include <iostream>
#include "intPair.hpp"

IPair::IPair()
{ }

IPair::IPair(int a, int b) : first(a), second(b)
{ }

void IPair::print() const
{
	std::cout << "(" << first << ", " << second << ")" << std::endl;
}

void IPair::swap()
{
	int temp = first;
	first = second;
	second = temp;
}