#include <iostream>
#include "intPair.hpp"
#include "douPair.hpp"

int main()
{
	IPair ipair(3, 8);
	DPair dpair(3.0, 8.0);

	ipair.print();
	ipair.swap();
	ipair.print();

	std::cout << "\n\n";

	dpair.print();
	dpair.swap();
	dpair.print();

	return 0;
}