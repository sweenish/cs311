#include <iostream>
#include <random>

template<typename T>
void selectionSort(T arr[], int unsigned size);

// Constant to make changing array sizes nicer
const int SIZE = 10;

int main()
{
    typedef int TYPE;  // Makes it easier to switch types, but distribution would have to be changed
                       // to match up
    std::random_device rd;
    std::mt19937 engine(rd());
    std::uniform_int_distribution<int> dist(0, 200);

    TYPE arr[SIZE];
    for (TYPE &i : arr)
        i = dist(engine);

    for (TYPE i : arr)
        std::cout << i << std::endl;
    
    selectionSort(arr, SIZE);

    std::cout << "\n\n";
    for (TYPE i : arr)
        std::cout << i << std::endl;

    
    return 0;
}

template<typename T>
void selectionSort(T arr[], unsigned int size)
{
    /*
     * do for size of array size - 1 
     *      search array for smallest value
     *           where smallest value is located
     *      swap it with first element in range
     */
    
    for (unsigned int j = 0; j < size - 1; j++) {
        // This was the only change made to the algorithm; two lines moved
        // This establishes the basis before looking through the array
        T min = arr[j];
        unsigned int index = j;
        for (unsigned int i = j + 1; i < size; i++) {
            if (arr[i] < min) {
                min = arr[i];
                index = i;
            }
        }
        T temp;
        temp = arr[j];
        arr[j] = arr[index];
        arr[index] = temp;

        // Don't set up for next iteration here, as it is now done at the beginning
    }
}