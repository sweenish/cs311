#ifndef PAIRCLASS_HPP
#define PAIRCLASS_HPP

#include <iostream>
/*
 * This example has the entire class in the header file, which is the only standards compliant way to implement a 
 * template class. 
 */

// We need to forward declare the class, specifying it as a template, so that we can forward declare the friend 
// function
template<typename T> class Pair;

// NOTE: No friend keyword, just the prototype of the function
template<typename T> std::ostream& operator<<(std::ostream& out, const Pair<T> &rhs);


template<class T>
class Pair {
public:
	Pair();
	Pair(T a, T b);
	void print() const;
	void swap();
    
    // Function declared again in the class, as a friend
    // NOTE the extra carat braces. Can be empty as well. Placing T inside locks the friend function to the same type 
    // as the object
    // https://en.cppreference.com/w/cpp/language/friend
    friend std::ostream& operator<< <T>(std::ostream& out, const Pair<T> &rhs);
	// {
	// 	out << "(" << rhs.first << ", " << rhs.second << ")";

	// 	return out;
	// }
private:
	T first;
	T second;
};


// Implementation
template<class T>
Pair<T>::Pair()
{
	first = T();
	second = T();
}

template<class T>
Pair<T>::Pair(T a, T b) : first(a), second(b)
{ }

template<class T>
void Pair<T>::print() const
{
	std::cout << "(" << first << ", " << second << ")" << std::endl;
}

template<class T>
void Pair<T>::swap()
{
	T temp = first;
	first = second;
	second = temp;
}

// Implementation of template friend of Pair function largely looks like we would expect any template function to look
// All extra specials are taken care of by this point
template<typename T>
std::ostream& operator<<(std::ostream& out, const Pair<T> &rhs)
{
    out << "(" << rhs.first << ", " << rhs.second << ")";

    return out;
}


int main()
{
    Pair<int> one(1, 2);
    Pair<double> two(3.14, 4.78);

    std::cout << one << std::endl << two << std::endl;

    return 0;
}

#endif