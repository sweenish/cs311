template<class T>
Pair<T>::Pair()
{
	first = T();
	second = T();
}

template<class T>
Pair<T>::Pair(T a, T b) : first(a), second(b)
{ }

template<class T>
void Pair<T>::print() const
{
	std::cout << "(" << first << ", " << second << ")" << std::endl;
}

template<class T>
void Pair<T>::swap()
{
	T temp = first;
	first = second;
	second = temp;
}