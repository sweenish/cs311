#include <iostream>

class Foo {
public:
	Foo();
	Foo(const Foo& rhs);
	~Foo();
	int* getAddr() const;
	int getVal() const;
	void setVal(int val);
	Foo& operator=(const Foo& rhs);
	friend std::ostream& operator <<(std::ostream &lhs, const Foo &rhs);
private:
	int *ptr;
};

const Foo giveBack(const Foo& obj)
{
	return obj;
}

int main()
{
	Foo foo;

	std::cout << "foo: " << foo << std::endl;

	{	// SCOPING
		Foo bar;
		bar = foo;
		std::cout << "bar: " << bar << std::endl;
		bar.setVal(99);
		std::cout << "bar: " << bar << std::endl;
	}

	std::cout << "foo: " << foo << std::endl;

	Foo three = giveBack(foo);

	return 0;
}

Foo::Foo()
{
	ptr = new int(42);
}

Foo::Foo(const Foo& rhs)
{
	std::cout << "COPY CONSTRUCTOR\n";
	ptr = new int( *(rhs.ptr) );
}

Foo::~Foo()
{
	delete ptr;
	ptr = nullptr;
}

int* Foo::getAddr() const
{
	return ptr;
}

int Foo::getVal() const
{
	return *ptr;
}

void Foo::setVal(int val)
{
	*ptr = val;
}

Foo& Foo::operator=(const Foo& rhs)
{
	std::cout << "ASSIGNMENT\n";
	if (!(this == &rhs)) {
		*ptr = *(rhs.ptr); 
	}

	return *this;
}

std::ostream& operator <<(std::ostream &lhs, const Foo & rhs)
{
	lhs << "(" << rhs.ptr << ", " << *(rhs.ptr) << ")";

	return lhs;
}